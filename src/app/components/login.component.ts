import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { UserService } from "../services/user.service" 

@Component({
	selector: 'login',
	templateUrl: '../views/login.html',
	providers: [UserService]
})
export class LoginComponent implements OnInit {
	public title: string;
	public user;
	public identity;
	public token;
	
	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService
		){
		this.title = "Login";
		this.user = {
			email: "",
			password: "",
			gethash: false
		};
	}
	
	ngOnInit(){
		console.log({"LoginComponent": "El componente ha sido cargado"});
		console.log({"LoginComponent": JSON.parse(localStorage.getItem('identiy'))});
	}

	onSubmit(params){
		console.log({onsubmit: this.user});
		if(params === true){
			this._userService.singup(this.user).subscribe(
				respose => {
					this.identity = respose;
					console.log({respuesta: this.identity});
					if(!this.identity.status){
						localStorage.setItem('identity', JSON.stringify(this.identity));
					}
				},
				error => {
					console.log({respuesta: error});
				}
				);
		}
		

	}
}