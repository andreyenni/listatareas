import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router"; 

@Component({
	selector: 'register',
	templateUrl: '../views/register.html'
})
export class RegisterComponent implements OnInit {
	public title: string;
	
	constructor(){
		this.title = "Componente Register";
	}
	
	ngOnInit(){
		console.log({"RegisterComponent": "El componente ha sido cargado"});
	}
}