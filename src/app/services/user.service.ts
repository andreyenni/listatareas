import { Injectable } from '@angular/core';
import { Http, Response, Headers,  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class UserService {
	public url: string;
	public identity;
	public token;

	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	singup(params){
		let jsonParams = JSON.stringify(params);
		let headers = new Headers({"Content-Type": "aplication/x-www-form-urlencoded"});
		let respose = this._http.get(this.url, params).map(res => res.json());
		console.log({servicio: respose});
		return respose;
	}
}